from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import login_required

from .models import Livro, Emprestimo, Contato

@login_required
def index(request):
    lista_livros = Livro.objects.all()
    template = loader.get_template('inicio.html')
    context = {
        'lista_livros': lista_livros
    }
    return HttpResponse(template.render(context, request))

@login_required
def emprestimos(request):

    lista_emprestimos = Emprestimo.objects.all().order_by('id')
    template = loader.get_template('emprestimos.html')
    context = {
        'lista_emprestimos':lista_emprestimos
    }

    return HttpResponse(template.render(context, request))

@login_required
def contatos(request):

    lista_contatos = Contato.objects.all()
    template = loader.get_template('contatos.html')
    context = {
        'lista_contatos':lista_contatos
    }

    return HttpResponse(template.render(context, request))