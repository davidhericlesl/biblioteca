from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='indexlib'),
    path('emprestimos/', views.emprestimos, name='emprestimos'),
    path('contatos/', views.contatos, name='contatos'),
]
