from django.db import models
# Create your models here.

class Livro(models.Model):
    titulo = models.CharField(max_length=100)
    resumo = models.TextField()
    capa = models.TextField()
    def __str__(self):
        return self.titulo

class Contato(models.Model):
    nome = models.CharField(max_length=30)
    telefone = models.CharField(max_length=11)
    email = models.CharField(max_length=50)
    def __str__(self):
        return self.nome

class Emprestimo(models.Model):
    livro = models.ForeignKey(Livro, on_delete=models.CASCADE)
    contato = models.ForeignKey(Contato, on_delete=models.CASCADE, default=1)
    data = models.DateTimeField('Data de emprestimo')
    status = models.BooleanField(default=False)